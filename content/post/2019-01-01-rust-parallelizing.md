---
title: "Rust Parallelizing"
date: 2019-01-01T10:30:15-06:00
tags: ["rust", "code"]
---

The beginnings of my project [`rust-domain-coloring`](https://gitlab.com/pmodl/rust-domain-coloring) came out of personal curiosity.
I independently realized that with hue and brightness,
an image can contain information of the four dimensions needed to display a function
$f: \mathbb C \rightarrow \mathbb C$.
From my previous experience coding with iterating over pixels in an image with Newton's Fractal,
I wanted this to be parallelized.
Hence, I decided to write it in rust.

<!--more-->

Of course, as I started writing, it became abundantly clear that I didn't grok rust.
I had used POSIX threads (pthreads) to parallelize in C before,
and probably wrote code that was completely jank and could break in unexpected ways --
or at least, that's what the Rust compiler has been telling me about my attempts to parallelize in Rust.

When beginning with Rust, the trait system was immediately attractive to me.
It was the one thing I did understand (probably thanks to my dabbling in Haskell).
The trait system is just the tip of the iceberg:
to paralleize code in Rust, one needs to understand ownership.

```rust
fn c64_slice<F: Fn(Complex64) -> Complex64 + Send + 'static + Copy> (fz: &mut Vec<Complex64>, region: &Region, f: &F, start: usize, end: usize) {
	for idx in start..end {
		fz.push(f(region.c64_at(idx)));
	}
}

pub fn c64_grid<F: Fn(Complex64) -> Complex64 + Send + 'static + Copy> (fz: &mut Vec<Vec<Complex64>>, region: Arc<Region>, f: &F, thread_count: usize) {
	let total = region.width * region.height;
	let seg_len = total / thread_count;

	// channel to send/receive vectors
	let (tx, rx): (_, mpsc::Receiver<(usize, Vec<Complex64>)>) = mpsc::channel();

	for i in 0..thread_count {
		let tx = tx.clone();
		let f = f.clone();
		let r = region.clone();
		thread::spawn(move || {
			let mut subvec: Vec<Complex64> = Vec::new();
			let end = if thread_count > i + 1 {
				seg_len * (i + 1)
			} else {
				r.width * r.height // last thread
			};
			// c64_slice borrows subvec
			c64_slice(&mut subvec, &r, &f, seg_len * i, end);
			// tx.send takes ownership of subvec
			tx.send((i, subvec)).unwrap();
		});
	}

	let mut received: Vec<(usize, Vec<Complex64>)> = Vec::new();
	for _ in 0..thread_count {
		if let Ok(val) = rx.recv() {
			received.push(val)
		}
	}
	received.sort_by(|(a, _), (b, _)| a.cmp(b));
	for (_, val) in received {
		fz.push(val);
	}
}
```

This is my current implementation of parallel mapping a complex function over a plane.
It has the limitation of the function being `'static`,
but this (hopefully) can be replaced with `Arc`s like I have with the `Region`.

I  want to focus on these two lines:
```rust
// c64_slice borrows subvec
c64_slice(&mut subvec, &r, &f, seg_len * i, end);
// tx.send takes ownership of subvec
tx.send((i, subvec)).unwrap();
```

Here, `subvec` is a vector of complex numbers.
The imput to each iteration of the function is provided by a call to `Region.c64_at(index)`.
My struct Region has this method which handles translation
from a one-dimensional index to associated input complex number at that pixel index.
Note the difference between the way the function is called with `subvec`.
In `c64_slice`, a **mutable reference** is passed to the function. c64 borrows subvec,
so ownership returns to the thread's closure when `c64_slice` returns.

However, when the subvec has been written to completely, ownership is ceded to `tx.send()`.
`tx` is a `Sender`, the sending half of Rust's asynchronous `channel` type.
We use `tx` to transfer ownership *from* the child thread *to* the main thread.
There is a corresponding `Reciever` in the main thread, which receives the vector.
It seems expensive to pass a huge vector by value through the channel,
but a vector is only a pointer and a length.
The compiler will free the vector (and it's data) when the *main thread* is finished with it.

The Rust fundamentals of lifetime and ownership are integral to writing any code in Rust.
Because the compiler has to keep track of ownership,
we have to tell the compiler who has access to every object.
The types `Rc` (reference count) and `Arc` (atomic reference count)
are useful to share data between multiple threads.
In fact, `Arc<Region>` is used here to ensure that the `Region` can be used by every thread independently without doing a deep clone.
The reference count increases every time the `Arc` is cloned, and decremented every time a clone goes out of scope.
The destructor is called on `Region` when the reference count reaches zero.

This kind of behavior is customary in Rust.
Unlike other systems languages, Rust provides memory management.
But unlike garbage-collected languages, references are traced at compile-time, not at runtime.
This is why programs built with Rust are as fast as they are; this is why programmers love Rust.

I may be spending more time with Rust in the near future.
