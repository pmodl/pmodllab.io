---
title: Setting up Neovim for LaTeX editing
subtitle: ""
date: 2018-08-06
tags: ["LaTeX", "neovim"]
---

I love building a good work environment, probably more than working in it.
I will find something about my workflow that doesn't work right, and try and fix it immediately.
As a Mathematician, I use LaTeX to write everything (documents generated through TeX always look gorgeous without much work).

`<sidenote>`*Built-in LaTeX parsing is actually one of the reasons I gravitated to Hugo for my website in the first place.*`</sidenote>`

<!--more-->


### TL;DR:

`$XDG_CONFIG_HOME/nvim/init.vim`
```none
" map leader
let maplocalleader = ","
" install vimtex
plugin-manager-of-choice 'lervag/vimtex'
" install syntastic
plugin-manager-of-choice 'vim-syntastic/syntastic'

" load vimtex
let g:tex_flavor='latex'
" enable folding
let g:vimtex_fold_enabled=1
" only fold secions
let  g:vimtex_fold_types = {
           \ 'envs' : {
           \   'whitelist' : [''],
           \ },
\}
```
`~/.latexmkrc`
```perl
$pdf_mode = 1;
$pdf_previewer = "start zathura %S 2>/dev/null";
$pdflatex = 'xelatex --shell-escape %O %S';
$out_dir = 'build';
$preview_continuous_mode = 1;
```
