---
title: "Gitlab Pages and LaTeX"
date: 2019-08-23T14:55:21-05:00
draft: true
---

Gitlab's continuous integration has some interesting benefits for publishing LaTeX documents.
There are guides out there for using CI to build pdfs,
and guides out there for using Pages to deploy pdfs on Gitlab Pages.
I've decided to go just a little bit further,
and add a small `index.html` in my setup.


<!--more-->

A few key points:

- A repo hosted at https://gitlab.com/<user>/<user>.gitlab.io
will become the root of http://<user>.gitlab.io
- A repo hosted at under https://gitlab.com/<user>/other
will publish at http://<user>.gitlab.io/other, with one exception.
- If <user>.gitlab.io publishes a directory /other,
then that directory will override the root directory of other.

After a bit of reading, I found that `tree -H` will produce an html page
suitable as an `index.html`.

`.gitlab-ci.yml`
```yaml
build:
  image: blang/latex:latest
  script:
    - latexmk -pdflatex='xelatex --shell-escape %O %S' -pdf *.tex
  artifacts:
    paths:
      - "*.pdf"
pages:
  image: alpine
  stage: deploy
  script:
    - apk add --no-cache tree
    - mkdir public
    - mv *.pdf public
    - tree -H '.' -I '*.html' -L 1 --noreport --charset utf-8 public > public/index.html
  artifacts:
    paths:
      - public
  only:
    - master
```
